'use strict';
var themeName   = 'rooom',
    gulp        = require('gulp'),
    watch       = require('gulp-watch'),
    sass        = require('gulp-sass'),
    cssmin      = require('gulp-cssmin'),
    babel       = require('gulp-babel'),
    uglify      = require('gulp-uglify-es').default,
    rename      = require('gulp-rename'),
    concat      = require('gulp-concat'),
    pipeline    = require('readable-stream').pipeline,
    buildFolder = themeName,
    scriptsArray= [
        'rooom/assets/js/swiper.js',
        'rooom/assets/js/aos.js',
        'rooom/assets/js/jquery.paroller.js',
        'rooom/assets/js/mousewheel-smoothscroll.js',
        'rooom/assets/js/general.js'
    ];

gulp.task('styles',  function () {
    return gulp.src('rooom/assets/sass/**/*.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest(buildFolder + '/assets/css'))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(buildFolder + '/assets/css/'));    
});

gulp.task('scripts', function () {
    return gulp.src(scriptsArray).pipe(concat('all.js'))
            .pipe(rename({suffix: '.min'}))
            .pipe(uglify())
            .pipe(gulp.dest(buildFolder + '/assets/js/'));
});

gulp.task('theme', function() {
    return gulp.src(buildFolder + '/**/*')
            .pipe(gulp.dest(themeName));
});

gulp.task('default', gulp.series(
    gulp.parallel(
    'styles',
    'scripts'
    )
));

gulp.task('watch', function () {
    gulp.watch('rooom/assets/sass/**/*.scss', gulp.series('styles'));

    gulp.watch(scriptsArray, gulp.series('scripts'));
});