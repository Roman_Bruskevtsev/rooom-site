<?php
/**
 *
 * @package WordPress
 * @subpackage Rooom
 * @since 1.0
 * @version 1.0
 */

get_header(); 

get_template_part( 'template-parts/page/content', 'title' );

if( have_rows('content') ):

	while ( have_rows('content') ) : the_row();

		if( get_row_layout() == 'projects_section' ) :
			get_template_part( 'template-parts/page/content', 'projects-section' );
		elseif( get_row_layout() == 'projects_blocks_section' ) :
			get_template_part( 'template-parts/page/content', 'projects-blocks-section' );
		elseif( get_row_layout() == 'testimonals_section' ) :
			get_template_part( 'template-parts/page/content', 'testimonals-section' );
		elseif( get_row_layout() == 'contact_section' ) :
			get_template_part( 'template-parts/page/content', 'contact-section' );
		elseif( get_row_layout() == 'large_title' ) :
			get_template_part( 'template-parts/page/content', 'large-title' );
		elseif( get_row_layout() == 'about_us_blocks' ) :
			get_template_part( 'template-parts/page/content', 'about-us-blocks' );
		elseif( get_row_layout() == 'what_we_do_section' ) :
			get_template_part( 'template-parts/page/content', 'what-we-do-section' );
		elseif( get_row_layout() == 'partners_section' ) :
			get_template_part( 'template-parts/page/content', 'partners-section' );
		elseif( get_row_layout() == 'price_form_section' ) :
			get_template_part( 'template-parts/page/content', 'price-form-section' );
		elseif( get_row_layout() == 'rooom_section' ) :
			get_template_part( 'template-parts/page/content', 'rooom-section' );
		endif;

	endwhile;

endif;

get_footer();