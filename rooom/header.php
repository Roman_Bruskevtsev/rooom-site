<?php
/**
 *
 * @package WordPress
 * @subpackage Rooom
 * @since 1.0
 * @version 1.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php wp_head(); ?>
</head>
<body <?php body_class('load'); ?>>
    <?php wp_body_open(); 
    get_template_part( 'template-parts/header/preloader' ); ?>
    <header data-aos="fade-down" data-aos-duration="600">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6 col-md-4 col-lg-2 col-xl-2">
                    <?php 
                    $logo = get_field('logo', 'option');
                    if( $logo ) { ?>
                    <a class="logo float-left" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo $logo['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                    </a>
                    <?php } ?>
                </div>
                <div class="col-6 col-md-8 col-lg-7 col-xl-5">
                    <?php if( has_nav_menu('main') ) { 
                        wp_nav_menu( array(
                            'theme_location'        => 'main',
                            'container'             => 'nav',
                            'container_class'       => 'main__nav float-left'
                        ) ); ?>
                        <div id="show__menu" class="menu__btn cursor__hover float-left">
                            <span></span><span></span><span></span>
                            <span></span><span></span><span></span>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-lg-3 col-xl-5">
                    <div class="right__block float-right">
                    <?php if( get_field('work_information_text', 'option') ) { ?>
                        <div class="work__information float-left"><?php the_field('work_information_text', 'option'); ?></div>
                    <?php } 
                    if( get_field('call_back_button_text', 'option') ) { ?>
                        <button class="btn btn__dark show__popup float-left"><span><?php the_field('call_back_button_text', 'option'); ?></span></button>
                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="menu__wrapper">
        <?php if( has_nav_menu('main') ) { ?>
        <div class="menu__block menu__side left">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-8">
                        <?php wp_nav_menu( array(
                            'theme_location'        => 'main',
                            'container'             => 'nav',
                            'container_class'       => 'main__nav'
                        ) ); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="menu__side right">
            <div class="contact__block">
                <?php if( get_field('work_information_text', 'option') ) { ?>
                    <div class="work__information"><?php the_field('work_information_text', 'option'); ?></div>
                <?php } 
                if( get_field('phone', 'option') ) { ?>
                    <a class="phone" href="tel:<?php the_field('phone', 'option'); ?>"><?php the_field('phone', 'option'); ?></a>
                <?php } 
                if( get_field('email', 'option') ) { ?>
                    <a class="email" href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a>
                <?php } ?>
            </div>
        </div> 
    </div>
    <main>