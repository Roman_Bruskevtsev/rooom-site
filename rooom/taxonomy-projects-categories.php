<?php
/**
 *
 * @package WordPress
 * @subpackage Rooom
 * @since 1.0
 * @version 1.0
 */
get_header(); 

$taxonomy = get_queried_object()->term_id;
$args = array(
	'posts_per_page' 	=> -1,
	'post_type' 		=> 'project',
	'tax_query' 		=> array(
		array(
			'taxonomy' => 'projects-categories',
			'field'    => 'id',
			'terms'    => $taxonomy
		)
	)
);
$query = new WP_Query( $args );	
$published_posts = (int) $query->found_posts < 10 ? '0'.$query->found_posts : $query->found_posts; ?>

<section class="page__wrapper">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-10 col-xl-8">
				<div class="projects__nav" data-aos="fade-up" data-aos-duration="600">
					<div class="view__bar">
						<div class="slider active cursor__hover"><?php _e('Slides', 'rooom'); ?></div>
						<div class="grid cursor__hover"><?php _e('Grid', 'rooom'); ?></div>
					</div>
					<?php if( has_nav_menu('projects-nav') ) { ?>
					<?php wp_nav_menu( array(
                        'theme_location'        => 'projects-nav',
                        'container'             => 'nav',
                        'container_class'       => 'projects__menu'
                    ) ); ?>
					<?php } ?>
				</div>
				<?php if ( $query->have_posts() ) { ?>
				<div class="archive__slider active swiper-container" data-aos="fade-up" data-aos-duration="600">
					<div class="swiper-wrapper">
						<?php while ( $query->have_posts() ) { $query->the_post(); 
							$background = get_field('thumbnails')['wide'] ? ' style="background-image: url('.get_field('thumbnails')['wide']['url'].');"' : ''; ?>
						<div class="swiper-slide">
							<a href="<?php the_permalink(); ?>" class="project__block cursor__scroll"<?php echo $background; ?>>
								<div class="content">
									<?php if( get_field('location') ) { ?><p><?php the_field('location'); ?></p><?php } ?>
									<?php if( get_field('short_title') ) { ?><h5><?php the_field('short_title'); ?></h5><?php } ?>
									<span><?php _e('Projects details', 'rooom'); ?></span>
								</div>
							</a>
						</div>
						<?php } wp_reset_postdata(); ?>
					</div>
					<div class="projects__count"><span class="current">01</span>/<span class="total"><?php echo $published_posts; ?></span></div>
				</div>
				<div class="archive__grid">
					<div class="row">
						<?php while ( $query->have_posts() ) { $query->the_post(); 
						$background = get_field('thumbnails')['wide'] ? ' style="background-image: url('.get_field('thumbnails')['wide']['url'].');"' : ''; ?>
						<div class="col-md-6">
							<a href="<?php the_permalink(); ?>" class="project__block" data-aos="fade-up" data-aos-duration="600"<?php echo $background; ?>>
								<div class="content">
									<?php if( get_field('location') ) { ?><p><?php the_field('location'); ?></p><?php } ?>
									<?php if( get_field('short_title') ) { ?><h5><?php the_field('short_title'); ?></h5><?php } ?>
									<span><?php _e('Projects details', 'rooom'); ?></span>
								</div>
							</a>
						</div>
					<?php } wp_reset_postdata(); ?>
					</div>
				</div>
				<?php } else { ?>
					<div class="no__content text-center">
						<h2><?php _e('Coming soon...', 'rooom'); ?></h2>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer();