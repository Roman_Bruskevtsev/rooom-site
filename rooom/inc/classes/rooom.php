<?php 

class RoomClass {
	public function __construct(){
		$this->actions_init();
	}

	public function actions_init(){
		add_action( 'wp_ajax_load_posts', array( $this, 'load_posts' ) );
        add_action( 'wp_ajax_nopriv_load_posts', array( $this, 'load_posts' ) );
	}

	public function __return_false() {
        return false;
    }

    public function load_posts(){
    	$paged = $_POST['page'];
    	$posts_per_page = get_option('posts_per_page');
    	$category = $_POST['category'];
    	$output = '';
    	$args = array(
			'post_type'			=> 'post',
			'posts_per_page' 	=> $posts_per_page,
			'post_status'		=> 'publish',
			'paged'				=> $paged
		);

		if($category != '*') $args['cat'] = $category;

		$query = new WP_Query( $args );
		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) { $query->the_post();
				$output .= get_template_part( 'template-parts/post/content', 'normal-load' );
			} 
		}
		wp_reset_postdata();
		wp_die();
    }
}

$rooom = new RoomClass();