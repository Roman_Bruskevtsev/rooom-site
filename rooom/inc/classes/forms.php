<?php
class FormsClass {
	public function __construct(){
		$this->scriptsDir = get_theme_file_uri().'/inc/classes/assets/js';
        $this->stylesDir = get_theme_file_uri().'/inc/classes/assets/css';

		$this->actions_init();
	}

	public function actions_init(){
		add_action( 'wp_ajax_form_validation', array( $this, 'form_validation' ) );
        add_action( 'wp_ajax_nopriv_form_validation', array( $this, 'form_validation' ) );

        add_filter( 'wp_mail_from_name', array( $this, 'email_sender_name' ) );
        add_filter( 'wp_mail_from', array( $this, 'sender_email' ) );
	}

	public function email_sender_name($original_email_from){
		return get_bloginfo('name');
	}

	public function sender_email(){
		return 'rooom.ds@gmail.com';
	}

	public function email_header($width = 800){
		$output = '
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
            <html>
            <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <link rel="profile" href="http://gmpg.org/xfn/11">
            </head>
            <body style="padding: 80px 0 120px; margin: 0; font-family: Roboto,RobotoDraft,Helvetica,Arial,sans-serif; background-color: #F5F5F5;">
                <table cellspacing="0" cellpadding="0" border="0" align="center" style="width:'.$width.'px; max-width:'.$width.'px; border: 1px solid #CACACA; padding: 50px 70px 10px; background-color: #fff;">
                    <thead>
                        <tr>
                            <th>
                                <a href="https://rooom.agency" target="_blank" style="display: block; text-align: left;">
                                    <img src="'.get_theme_file_uri( '/assets/images/logo.png' ).'" width="150" height="29" alt="'.get_bloginfo('name').'" style="margin: 0 0 40px;">
                                </a>
                            </th>
                            <th style="vertical-align: top;">
                            	<p style="color: #2C2C2D; margin:0; font-family: Roboto,RobotoDraft,Helvetica,Arial,sans-serif; font-weight: 400; font-size: 16px; line-height: 29px; text-align: right; letter-spacing: 0.27px;">'.__('Interior Design Studio', 'rooom').'</p>
                            </th>
                        </tr>
                    </thead>';

        return $output;
	}

	public function customer_email_body(){
		$output = '	<tbody>
				    	<tr>
				    		<td colspan="2">
				    			<h1 style="color: #2C2C2D; font-size: 28px; font-weight: 400; line-height: 36px; margin-top: 40px; margin-bottom: 10px">'.__('We have received your message!', 'rooom').'</h1>
				    		</td>
				    	</tr>
				    	<tr>
				    		<td colspan="2">
				    			<p style="font-size: 16px; color: #2C2C2D; letter-spacing: 0.4px; line-height: 24px; margin-bottom: 40px;">'.__('Thank you, our managers will contact you shortly!', 'rooom').'</p>
				    		</td>
				    	</tr>
					</tbody>';

		return $output;
	}

	public function admin_email_body($fields){
		$output = '	<tbody>
				    	<tr>
				    		<td colspan="2">
				    			<h1 style="color: #2C2C2D; font-size: 28px; font-weight: 400; line-height: 36px; margin-top: 40px; margin-bottom: 20px">'.__('We received a new request!', 'rooom').'</h1>
				    		</td>
				    	</tr>
				    	<tr>
				    		<td colspan="2">
				    			<p style="font-size: 16px; color: #2C2C2D; letter-spacing: 0.4px; line-height: 24px; margin-bottom: 40px;">'.__('Form details:', 'rooom').'</p>
				    		</td>
				    	</tr>';
		foreach ($fields as $field) {
			if( !empty($field->value) && ($field->name !='admin-email') ){
				if( $field->name == 'file' ){
					$output .= '<tr>
					    		<td>
					    			<p style="font-size: 16px; color: #2C2C2D; line-height: 24px; margin: 0 0 5px 0;">'.$field->name.':</p>
				    			</td>
					    		<td>
					    			<a href="'.$field->url.'" target="_blank" style="font-size: 16px; color: color: rgba(255,255,255,0.7); line-height: 24px; margin: 0 0 15px 0;">'.$field->value.'</a>
					    		</td>
					    	</tr>';
				} else {
					$output .= '<tr>
					    		<td>
					    			<p style="font-size: 16px; color: #2C2C2D; line-height: 24px; margin: 0 0 5px 0;">'.$field->name.':</p>
				    			</td>
					    		<td>
					    			<p style="font-size: 16px; color: #2C2C2D; line-height: 24px; margin: 0 0 5px 0;">'.$field->value.'</p>
					    		</td>
					    	</tr>';
				}
			}
		}
		$output .='	</tbody>';

		return $output;
	}

	public function email_footer(){
		$output = '
            		<tfoot>
            			<tr>
            				<td colspan="2" style="padding-top: 10px; border-top: 1px solid #BDBDBD;">
            					<h3 style="text-align: right; color: #2C2C2D; font-size: 20px; font-weight: bold; line-height: 28px; margin-bottom: 10px;">'.__('Follow us:', 'rooom').'</h3>
            				</td>
            			</tr>
            			<tr>
            				<td colspan="2" style="padding-bottom: 40px;">
            					<ul style="margin: 0; padding: 0; list-style: none; width: 100%; text-align: right;">
            						<li style="display: inline-block; margin: 0 0 0 30px;">
            							<a style="color: #2C2C2D; font-size: 13px; line-height: 24px; letter-spacing: 0.27px; text-decoration: none;" href="https://www.instagram.com/rooom_design_studio/" target="_blank">'.__('Instagram', 'rooom').'</a>
            						</li>
            						<li style="display: inline-block; margin: 0 0 0 30px;">
            							<a style="color: #2C2C2D; font-size: 13px; line-height: 24px; letter-spacing: 0.27px; text-decoration: none;" href="https://www.facebook.com/rooom.design.studio/" target="_blank">'.__('Facebook', 'rooom').'</a>
            						</li>
            					</ul>
            				</td>
            			</tr>
            		</tfoot>
	            </table>
	        </body>
	        </html>';
        return $output;
    }

	public function phone_validation($phone){
		$filtered_phone_number = filter_var( $phone, FILTER_SANITIZE_NUMBER_INT );
		$phone_to_check = str_replace('-', '', $filtered_phone_number);

		if (strlen($phone_to_check) < 10 || strlen($phone_to_check) > 14) {
	        return false;
	     } else {
	       return true;
	     }
	}

	public function form_validation(){
		if( wp_verify_nonce( $_POST['security'], 'security' ) ) {
			$fields = json_decode(stripslashes($_POST['fields']));
			$fieldsArray = [];
			$errors = false;
			$admin_email = get_option('admin_email');
			$user_email = '';

			foreach ($fields as $field) {
				$fieldArray = $field;
				$fieldArray->error = false;
				if ( $field->required ){
					if( $field->value == '' ){
						$fieldArray->error = __('This field is required, please fill it.', 'rooom');
						$errors = true;
					} else {
						if( $field->name == 'email' ){
							if( !filter_var( $field->value, FILTER_VALIDATE_EMAIL ) ){
								$fieldArray->error = __('Email is wrong', 'rooom');
								$errors = true;
							} else {
								$user_email = $field->value;
							}
						} elseif ( $field->name == 'phone' ){
							if( !$this->phone_validation($field->value) ){
								$fieldArray->error = __('Phone is wrong', 'rooom');
								$errors = true;
							}
						} elseif( $field->name == 'admin-email' ){
							$admin_email = $field->value;
						}
					}
				} else {
					if( $field->value != '' ){
						if( $field->name == 'email' ){
							if( !filter_var( $field->value, FILTER_VALIDATE_EMAIL ) ){
								$fieldArray->error = __('Email is wrong', 'rooom');
								$errors = true;
							} else {
								$user_email = $field->value;
							}
						} elseif ( $field->name == 'phone' ){
							if( !$this->phone_validation($field->value) ){
								$fieldArray->error = __('Phone is wrong', 'rooom');
								$errors = true;
							}
						} elseif( $field->name == 'admin-email' ){
							$admin_email = $field->value;
						}
					}
				}

				$fieldsArray[] = $fieldArray;
			}

			if( $errors ) {
				wp_send_json_error( $fieldsArray, 400 );
			} else {

				if( isset($_FILES['file']) ){
					if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );  
					$uploads = wp_upload_dir();

					$folderPath = ABSPATH . 'wp-content/uploads/files';
	                if (!file_exists($folderPath)) {
	                    mkdir($folderPath, 0777, true);
	                }
	                $ext = end((explode('.', $_FILES['file']['name']))); 
	                $name = explode('.', $_FILES['file']['name'])[0];

	                if( $ext== 'pdf' || $ext== 'docx' || $ext== 'doc' ){
	                	$fileName = $name . '_'.time(). '.' . $ext;
	                    $userTempData = $folderPath . '/' . $fileName;

	     				$uploadFile = move_uploaded_file($_FILES['file']['tmp_name'], $userTempData);

	     				if($uploadFile){
	     					$filePath = esc_url( home_url( '/' ) ).'wp-content/uploads/files/' . $fileName;

	     					$fieldsArray[] = (object) array(
			                	'name'		=> 'file',
			                	'value'		=> $fileName,
			                	'url'		=> $filePath
			                );
	     				}
	                }
				}

				$adminHeaders = $userHeaders = "Content-type: text/html; charset=utf-8 \r\n";  
                $adminHeaders .= 'From: rooom.ds@gmail.com' . "\r\n";
                $userHeaders  .= 'From: rooom.ds@gmail.com' . "\r\n";

				$adminMessage = $this->email_header().$this->admin_email_body($fieldsArray).$this->email_footer();
				if( $user_email != '' ) $userMessage = $this->email_header().$this->customer_email_body().$this->email_footer();

				$adminMail = wp_mail( $admin_email, 'Rooom Design Studio', $adminMessage, $adminHeaders );
				if( $user_email != '' ) $userMail = wp_mail( $user_email, 'Rooom Design Studio', $userMessage, $userHeaders );

				if( $adminMail || $userMail ) {
					$success_page = get_field('success_page', 'option');
					$success_url = $success_page ? get_the_permalink( $success_page ) : '';
					$data = array(
						'text' => __('We have received your message and will contact you shortly!', 'rooom'),
						'url'  => urlencode($success_url)
					);
					wp_send_json_success( $data, 200 );
				} else {
					wp_send_json_error( __('Server error, please try again later', 'rooom'), 500 );
				}
			}
		}
	}
}

$forms = new FormsClass();