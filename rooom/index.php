<?php
/**
 *
 * @package WordPress
 * @subpackage Rooom
 * @since 1.0
 * @version 1.0
 */
get_header();

$posts_per_page = get_option('posts_per_page');
$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1; ?>
<section class="blog__section">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-8">
				<?php 
				get_template_part( 'template-parts/breadcrumbs' ); 
				get_template_part( 'template-parts/categories' ); 
				?>
			</div>
		</div>	
	<?php $args = array(
		'post_type'			=> 'post',
		'posts_per_page' 	=> $posts_per_page,
		'post_status'		=> 'publish',
		'paged'				=> $paged
	);

	$query = new WP_Query( $args );
	$post_count = $query->post_count;

	if ( $query->have_posts() ) { ?>
		<div class="row posts__row">
		<?php 
		$i = 1;
		while ( $query->have_posts() ) { $query->the_post();
			if( is_sticky( get_the_ID() ) ) { ?>
				<div class="col-lg-7 col-xl-8"><?php get_template_part( 'template-parts/post/content', 'wide' ); ?></div>
				<div class="d-none d-lg-block col-lg-5 col-xl-4"><?php get_template_part( 'template-parts/post/content', 'sidebar' ); ?></div>
			<?php } 
			if( $post_count >= 10 ){
				if( $i == 8 ){ ?>
					<div class="col-lg-4">
						<div class="row">
							<div class="col-lg-12"><?php get_template_part( 'template-parts/post/content', 'normal' ); ?></div>
				<?php } elseif( $i == 9 ) { ?>
							<div class="col-lg-12"><?php get_template_part( 'template-parts/post/content', 'normal' ); ?></div>
						</div>
					</div>
				<?php } elseif ( $i == 10 ) { ?>
					<div class="col-lg-8"><?php get_template_part( 'template-parts/post/content', 'big' ); ?></div>
				<?php } elseif ( !is_sticky( get_the_ID() ) ) { ?>
					<div class="col-lg-4"><?php get_template_part( 'template-parts/post/content', 'normal' ); ?></div>
				<?php } 
			} else { ?>
				<div class="col-lg-4"><?php get_template_part( 'template-parts/post/content', 'normal' ); ?></div>
			<?php } 
		$i++ ; } ?>
		</div>
	<?php } else {
	} wp_reset_postdata(); ?>
	</div>
	<?php
	if( $query->max_num_pages > 1 && $query->max_num_pages != $paged ) { ?>
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<div class="load__posts cursor__hover" data-current="1" data-pages="<?php echo $query->max_num_pages; ?>" data-category="*"><?php _e('Show more', 'rooom'); ?></div>
				</div>
			</div>
		</div>
	<?php } ?>
</section>
<?php 
get_template_part( 'template-parts/footer/price-form' );
get_footer();