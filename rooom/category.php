<?php
/**
 *
 * @package WordPress
 * @subpackage Rooom
 * @since 1.0
 * @version 1.0
 */
get_header();
$category = get_queried_object();
$posts_per_page = get_option('posts_per_page');
$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1; ?>
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-lg-8">
			<?php 
			get_template_part( 'template-parts/breadcrumbs' ); 
			get_template_part( 'template-parts/categories' ); 
			?>
		</div>
	</div>	
<?php $args = array(
	'post_type'			=> 'post',
	'posts_per_page' 	=> $posts_per_page,
	'post_status'		=> 'publish',
	'paged'				=> $paged,
	'cat'				=> $category->term_id
);

$query = new WP_Query( $args );
$post_count = $query->post_count;

if ( $query->have_posts() ) { ?>
	<div class="row posts__row">
	<?php 
	while ( $query->have_posts() ) { $query->the_post(); ?>
		<div class="col-lg-4"><?php get_template_part( 'template-parts/post/content', 'normal' ); ?></div>
	<?php } ?>
	</div>
<?php } else {
} wp_reset_postdata(); ?>
</div>

<?php 
get_template_part( 'template-parts/footer/price-form' );
if( $query->max_num_pages > 1 && $query->max_num_pages != $paged ) { ?><div class="load__posts" data-pages="<?php echo $query->max_num_pages; ?>" data-category="<?php echo $category->term_id; ?>"></div><?php } ?>
<?php get_footer();