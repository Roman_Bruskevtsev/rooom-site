<?php
/**
 *
 * @package WordPress
 * @subpackage Rooom
 * @since 1.0
 * @version 1.0
 */
?>
    </main>
    <footer>
		<?php if( is_home() || is_category() || is_singular('post') ) get_template_part( 'template-parts/footer/copyright' ); ?>
    </footer>
    <?php get_template_part( 'template-parts/footer/popup' ); ?>
    <div class="cursor">
    	<div class="dot"></div>
    	<div class="circle"></div>
    </div>
    <?php if( is_page() || is_single() ) { ?>
    	<div class="page__scroll"><span></span></div>
    <?php } ?>
    <?php if( get_field('instagram', 'option') || get_field('behance', 'option') || get_field('facebook', 'option') ) { ?>
        <div class="social__links">
            <ul>
                <?php if( get_field('instagram', 'option') ) { ?>
                <li>
                    <a href="<?php echo get_field('instagram', 'option'); ?>" target="_blank" class="instagram"></a>
                </li>
                <?php } ?>
                <?php if( get_field('behance', 'option') ) { ?>
                <li>
                    <a href="<?php echo get_field('behance', 'option'); ?>" target="_blank" class="behance"></a>
                </li>
                <?php } ?>
                <?php if( get_field('facebook', 'option') ) { ?>
                <li>
                    <a href="<?php echo get_field('facebook', 'option'); ?>" target="_blank" class="facebook"></a>
                </li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>
    <?php wp_footer(); ?>
</body>
</html>