<?php 
$categories = get_categories();
$current_category = get_queried_object();
if( $categories ){
	echo '<div class="categories">';
	foreach ( $categories as $category ) {
		$active = $current_category->term_id == $category->term_id ? ' class="active"' : '';
		$color = get_field( 'category_color', 'category_'.$category->term_id ) ? get_field( 'category_color', 'post_tag_'.$category->term_id ) : '#2C2C2D';
		
		echo '<a'.$active.' href="'.get_tag_link($category->term_id).'" style="color: '.$color.'; border-color: '.$color.'">'.$category->name.'</a>';
	}
	echo '</div>';
}