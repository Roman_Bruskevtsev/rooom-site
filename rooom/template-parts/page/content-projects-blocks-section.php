<?php
$logo = get_sub_field('logo'); 
$projects = get_sub_field('projects');
?>
<section class="projects__blocks">
	<div class="container-fluid">
		<?php if( $logo ) {	?>
		<div class="row">
			<div class="col">
				<div class="logo__group">
					<?php if( $logo['logo'] ) { ?>
						<div class="img text__overflow">
							<img data-aos="fade-up" data-aos-duration="500" data-aos-delay="200" src="<?php echo $logo['logo']['url']; ?>" alt="<?php echo $logo['logo']['title']; ?>">
						</div>
					<?php } 
					if( $logo['subtitle_1'] || $logo['subtitle_2'] ) { ?>
						<div class="text__group">
							<div class="text__overflow">
								<h3 data-aos="fade-up" data-aos-duration="500" data-aos-delay="600"><?php echo $logo['subtitle_1']; ?></h3>
							</div>
							<div class="text__overflow">
								<h2 data-aos="fade-up" data-aos-duration="500" data-aos-delay="400" class="h1 stroke"><?php echo $logo['subtitle_2']; ?></h2>
							</div>
						</div>
					<?php } ?>
				</div>
				
			</div>
		</div>
		<?php }
		if( $projects ) { 
			$args = array(
				'posts_per_page' 	=> -1,
				'post_type' 		=> 'project',
				'post__in'			=> $projects,
				'orderby'			=> 'post__in'
			);
			$query = new WP_Query( $args );	
			if ( $query->have_posts() ) { 
				$i = 1;
				while ( $query->have_posts() ) { $query->the_post(); 

					if( $i % 2 == 0 ){ 
						get_template_part( 'template-parts/project/content', 'thumbnail-even' );
					} else {
						get_template_part( 'template-parts/project/content', 'thumbnail-odd' );
					}
				$i++;
				} 
				wp_reset_postdata();
			} 
		} ?>
	</div>
</section>