<section class="large__title">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-2"></div>
			<div class="col-lg-10">
				<div class="title">
					<h2><span class="paroller" data-paroller-type="foreground" data-paroller-direction="horizontal" data-paroller-factor="-0.8"><?php the_sub_field('title_1'); ?></span> <span class="paroller" data-paroller-type="foreground" data-paroller-direction="horizontal" data-paroller-factor="0.8"><?php the_sub_field('title_2'); ?></span></h2>
				</div>
			</div>
		</div>
	</div>
</section>