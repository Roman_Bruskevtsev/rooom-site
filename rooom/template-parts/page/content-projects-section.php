<section class="projects__section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-2"></div>
			<div class="col-lg-10">
				<?php if( get_sub_field('title') ) { ?>
				<div class="title" data-aos="fade-up" data-aos-duration="600" data-aos-delay="300">
					<h1><?php the_sub_field('title'); ?></h1>
					<ul>
						<li class="red"><?php the_sub_field('red_text'); ?></li>
						<li class="green"><?php the_sub_field('green_text'); ?></li>
						<li class="blue"><?php the_sub_field('blue_text'); ?></li>
					</ul>
				</div>
				<?php } 
				$categories = get_sub_field('choose_categories'); 
				if( $categories ){ ?>
				<nav class="categories__nav">
					<ul>
					<?php 
					$delay = 400;
					foreach ( $categories as $category ) { ?>
						<li>
							<a data-aos="fade-up" data-aos-duration="600" data-aos-delay="<?php echo $delay; ?>" href="<?php echo get_term_link( $category->term_id, $category->taxonomy ); ?>"><?php echo $category->name; ?></a>
						</li>
					<?php $delay+=200; } ?>
					</ul>
				</nav>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php 
	$slider = get_sub_field('images');
	if( $slider ) { ?>
	<div class="container-fluid relative nopadding left">
		<div class="row">
			<div class="col">
				<div class="projects__slider swiper-container">
					<div class="swiper-wrapper">
					<?php foreach ( $slider as $slide ) {
						$background = ' style="background-image: url('.$slide['url'].')"'; ?>
						<div class="swiper-slide"<?php echo $background; ?>></div>
					<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<div class="scrollbar">
			<div class="content">
				<div class="icon"></div>
				<div class="text">Гортайте вниз</div>
			</div>
		</div>
	</div>
	<?php } ?>
</section>