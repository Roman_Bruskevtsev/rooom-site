<section class="about__us__blocks">
	<span class="icon"><span class="scroll"></span></span>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-2"></div>
			<div class="col-xl-4">
				<?php 
				$left_side = get_sub_field('left_side'); 
				if( $left_side ) { ?>
				<div class="about__block left" data-aos="fade-right" data-aos-duration="500">
					<div class="title">
						<div class="content">
							<?php if( $left_side['number'] ) { ?><div class="number"><?php echo $left_side['number']; ?></div><?php } ?>
							<?php if( $left_side['title'] ) { ?><h5><?php echo $left_side['title']; ?></h5><?php } ?>
						</div>
					</div>
					<?php if( $left_side['image'] ) { 
						$background = ' style="background-image: url('.$left_side['image']['url'].');"'; ?>
					<div class="image"<?php echo $background; ?>>
						<!-- <img src="<?php echo $left_side['image']['url']; ?>" alt="<?php echo $left_side['image']['title']; ?>"> -->
					</div>
					<?php } ?>
					<?php if( $left_side['text'] ) { ?>
					<div class="text"><?php echo $left_side['text']; ?></div>
					<?php } ?>
				</div>
				<?php } ?>
			</div>
			<div class="col-xl-1"></div>
			<div class="col-xl-4">
				<?php 
				$right_side = get_sub_field('right_side'); 
				if( $right_side ) { ?>
				<div class="about__block right" data-aos="fade-left" data-aos-duration="500">
					<?php if( $right_side['text'] ) { ?>
					<div class="text"><?php echo $right_side['text']; ?></div>
					<?php } ?>
					<?php if( $right_side['image'] ) { 
						$background = ' style="background-image: url('.$right_side['image']['url'].');"'; ?>
					<div class="image"<?php echo $background; ?>>
						<!-- <img src="<?php echo $right_side['image']['url']; ?>" alt="<?php echo $right_side['image']['title']; ?>"> -->
					</div>
					<?php } ?>
					<div class="title">
						<div class="content">
							<?php if( $right_side['number'] ) { ?><div class="number"><?php echo $right_side['number']; ?></div><?php } ?>
							<?php if( $right_side['title'] ) { ?><h5><?php echo $right_side['title']; ?></h5><?php } ?>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>