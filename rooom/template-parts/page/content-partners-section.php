<section class="partners__section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-2"></div>
			<div class="col-md-4">
				<div class="title__block">
					<?php if( get_sub_field('partners_count') ) { ?><h3><?php the_sub_field('partners_count'); ?></h3><?php } ?>
					<?php if( get_sub_field('title') ) { ?><h5><?php the_sub_field('title'); ?></h5><?php } ?>
				</div>
			</div>
			<?php 
			$partners = get_sub_field('partners');
			if( $partners ) { ?>
			<div class="col-md-8 col-lg-6">
				<div class="partners__block">
					<div class="row">
					<?php foreach ( $partners as $partner ) { ?>
						<div class="col-md-6">
						<?php if( $partner['link'] ) { ?>
							<a class="partner" href="<?php echo $partner['link']; ?>">
								<?php if( $partner['logo'] ) { ?>
									<img src="<?php echo $partner['logo']['url']; ?>" alt="<?php echo $partner['logo']['title']; ?>">
								<?php } ?>
							</a>
						<?php } else { ?>
							<div class="partner">
								<?php if( $partner['logo'] ) { ?>
									<img src="<?php echo $partner['logo']['url']; ?>" alt="<?php echo $partner['logo']['title']; ?>">
								<?php } ?>
							</div>
						<?php } ?>
						</div>
					<?php } ?>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>