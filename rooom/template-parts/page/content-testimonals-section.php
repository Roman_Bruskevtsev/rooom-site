<?php 
$slider = get_sub_field('testimonials'); 
?>
<section class="testimonials__section">
	<?php if( get_sub_field('title') ) { ?>
	<div class="container-fluid">
		<div class="row">
			<div class="col">
				<div class="section__title">
					<h2 class="h1 stroke" data-aos="fade-left" data-aos-duration="1000"><?php the_sub_field('title'); ?></h2>
				</div>
			</div>
		</div>
	</div>
	<?php } 
	if( $slider ) { ?>
	<div class="container-fluid nopadding">
		<div class="row">
			<div class="col nopadding">
				<div class="testimonials__slider swiper-container cursor__scroll">
					<div class="swiper-wrapper">
						<?php foreach ( $slider as $slide ) { 
						$avatar = $slide['avatar'] ? ' style="background-image: url('.$slide['avatar']['url'].')"' : ''; ?>
						<div class="swiper-slide">
							<div class="content">
								<div class="avatar"<?php echo $avatar; ?>></div>
								<div class="text"><?php echo $slide['text']; ?></div>
								<div class="details">
									<?php if( $slide['author'] ) { ?><div class="author float-left"><?php echo $slide['author']; ?></div><?php } ?>
									<?php if( $slide['project'] ) { ?><a class="float-left" href="<?php echo get_permalink( $slide['project'] ); ?>"><?php _e('View the project', 'rooom'); ?></a><?php } ?>
								</div>
							</div>
						</div>
						<?php } ?>
						<div class="swiper-slide"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
</section>