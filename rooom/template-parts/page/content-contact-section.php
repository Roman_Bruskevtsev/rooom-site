<?php 
$map = get_sub_field('map');
$contacts = get_sub_field('contacts');
?>
<section class="contacts__section" id="contacts">
	<div class="container-fluid">
		<div class="row">
			<div class="col">
				<div class="section__title">
					<h2 class="h1 stroke" data-aos="fade-left" data-aos-duration="1000"><?php the_sub_field('title'); ?></h2>
				</div>
			</div>
		</div>
		<div class="row">
			<?php if( $map['address'] && $map['latitude'] && $map['longitude'] && $map['zoom'] ) { ?>
			<div class="col-lg-6 col-xl-5">
				<div id="google__map" class="map" data-aos="fade-up" data-aos-duration="1000" data-str="<?php echo $map['address']; ?>" data-latitude="<?php echo $map['latitude']; ?>" data-longitude="<?php echo $map['longitude']; ?>" data-zoom="<?php echo $map['zoom']; ?>" data-marker="<?php echo get_theme_file_uri().'/assets/images/icons/marker.png'; ?>"></div>
			</div>
			<?php } ?>
			<div class="col-lg-6 col-xl-6">
				<div class="contacts">
					<?php if( $contacts['text'] ) { ?>
						<div class="text" data-aos="fade-left" data-aos-duration="1000"><?php echo $contacts['text']; ?></div>
					<?php } 
					if( $contacts['phone'] ) { ?>
					<a href="tel:<?php echo $contacts['phone']; ?>" class="phone" data-aos="fade-left" data-aos-duration="1000"><?php echo $contacts['phone']; ?></a>
					<?php }
					if( $contacts['email'] ) { ?>
					<a href="mailto:<?php echo $contacts['email']; ?>" class="email" data-aos="fade-left" data-aos-duration="1000"><?php echo $contacts['email']; ?></a>
					<?php } 
					if( $contacts['button_text'] ) { ?>
						<button class="btn btn__dark show__popup" data-aos="fade-left" data-aos-duration="1000"><span><?php echo $contacts['button_text']; ?></span></button>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>