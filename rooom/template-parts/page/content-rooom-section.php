<section class="rooom__section">
	<div class="content">
		<?php if( get_sub_field('title') ){ ?>
		<div class="title text-center">
			<h2><?php the_sub_field('title'); ?></h2>
		</div>
		<?php } ?>
		<div class="contact__block text-center">
			<div class="contact">
				<?php if( get_sub_field('contact_title') ){ ?><h3><?php the_sub_field('contact_title'); ?></h3><?php } ?>
				<?php if( get_sub_field('email') ){ ?>
					<a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a>
				<?php } ?>
				<?php if( get_sub_field('phone') ){ ?>
					<a href="mailto:<?php the_sub_field('phone'); ?>"><?php the_sub_field('phone'); ?></a>
				<?php } ?>
				<?php if( get_sub_field('button_label') ){ ?>
				<button class="btn btn__dark show__popup"><span><?php the_sub_field('button_label'); ?></span></button>
				<?php } ?>
			</div>
		</div>
	</div>
</section>