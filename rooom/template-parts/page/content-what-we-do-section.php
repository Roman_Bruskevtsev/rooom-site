<section class="what__do__section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-2"></div>
			<div class="col-lg-3">
				<?php if( get_sub_field('title') ) { ?>
				<div class="title" data-aos="fade-right" data-aos-duration="1000">
					<h2 class="h3"><?php the_sub_field('title'); ?></h2>
				</div>
				<?php } ?>
			</div>
			<div class="col-lg-7">
				<?php 
				$slider = get_sub_field('slider');
				$other_slide = get_sub_field('other_slide');
				if( $slider ) { ?>
				<div class="works__slider swiper-container" data-aos="fade-up" data-aos-duration="1000">
					<div class="swiper-wrapper">
						<?php foreach( $slider as $slide ) { 
						$background = $slide['background'] ? ' style="background-color:'.$slide['background'].'"' : '';
						$label = $slide['background'] ? ' style="-webkit-text-stroke-color:'.$slide['label_color'].'"' : ''; ?>
						<div class="swiper-slide">
							<div class="work">
								<?php if( $slide['image'] ) { ?>
								<div class="image">
									<img src="<?php echo $slide['image']['url']; ?>" alt="<?php echo $slide['image']['title']; ?>">
								</div>
								<?php } ?>
								<div class="content"<?php echo $background; ?>>
									<?php if( $slide['icon'] ) { ?><img src="<?php echo $slide['icon']['url']; ?>" alt="<?php echo $slide['icon']['title']; ?>"><?php } ?>
									<?php if( $slide['title'] ) { ?>
									<div class="title">
										<h3><?php echo $slide['title']; ?>
											<?php if( $slide['subtitle'] ) { ?><span class="large stroke"<?php echo $label; ?>><?php echo $slide['subtitle']; ?></span><?php } ?>
										</h3>
									</div>
									<?php } ?>
									<?php if( $slide['text'] ) { ?><div class="text"><?php echo $slide['text']; ?></div><?php } ?>
								</div>
							</div>
						</div>
						<?php } 
						if( $other_slide ) { 
						$slide = get_sub_field('slide');
						$background = $slide['background'] ? ' style="background-color:'.$slide['background'].'"' : '';
						?>
						<div class="swiper-slide">
							<div class="other"<?php echo $background; ?>>
								<?php if( $slide['title'] ) { ?><h3><?php echo $slide['title']; ?></h3><?php } 
								if( $slide['url'] ) { ?><a href="<?php echo $slide['url']; ?>"><?php } 
								if( $slide['icon'] ) { ?>
									<img src="<?php echo $slide['icon']['url']; ?>" alt="<?php echo $slide['icon']['title']; ?>">
								<?php }
								if( $slide['url'] ) { ?></a><?php } 
								echo $slide['text']; ?>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>