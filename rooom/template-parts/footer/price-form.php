<?php 
$archive_link = get_post_type_archive_link('project'); 
$form = get_field('project_price_form', 'option');
?>
<section class="price__form__section">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-7">
				<div class="form__block">
					<form onsubmit="return false;">
						<?php wp_nonce_field('security', 'form-security_'.wp_unique_id()); ?>
						<?php if( $form['title'] ) { ?><div class="title text-center"><h5><b><?php echo $form['title']; ?></b></h5></div><?php } ?>
						<div class="steps">
							<?php $step_1 = $form['step_1']; ?>
							<div class="step step__1 active">
								<?php if( $step_1['title'] ) { ?>
									<div class="name text-center"><h6><?php echo $step_1['title']; ?></h6></div>
								<?php } 
								$styles = $step_1['styles']; 
								if( $styles ) { ?>
								<div class="field__group">
								<?php foreach ( $styles as $style ) { 
									$image = $style['icon']; 
									$image_hover = $style['icon_hover']; ?>
									<label class="radio">
										<?php if( $style['name'] ) { ?>
											<input class="form__field" type="radio" name="design-type" value="<?php echo $style['name']; ?>">
										<?php } ?>
										<div class="icons">
											<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
											<img src="<?php echo $image_hover['url']; ?>" alt="<?php echo $image_hover['title']; ?>">
										</div>
										<span><?php echo $style['name']; ?></span>
									</label>
								<?php } ?>
								</div>
								<?php } ?>
							</div>
							<div class="step step__2">
								<?php 
								$step_2 = $form['step_2'];
								if( $step_2['title'] ) { ?>
									<div class="name text-center"><h6><?php echo $step_2['title']; ?></h6></div>
								<?php } ?>
								<div class="field__group inline">
									<label>
										<span class="title"><?php _e('Your name', 'rooom'); ?></span>
										<input class="form__field" required type="text" name="name" placeholder="<?php _e('Name', 'rooom'); ?>">
									</label>
									<label>
										<span class="title"><?php _e('Phone', 'rooom'); ?></span>
										<input class="form__field" required type="tel" name="phone" placeholder="<?php _e('096 1234 123', 'rooom'); ?>">
									</label>
									<label>
										<span class="title"><?php _e('Email', 'rooom'); ?></span>
										<input class="form__field" required type="text" name="email" placeholder="<?php _e('customer@mail.com', 'rooom'); ?>">
									</label>
								</div>
							</div>
						</div>
						<div class="steps__nav step__1">
							<div class="buttons">
								<button type="button" class="step__nav disabled prev__step"></button>
								<button type="button" class="step__nav disabled next__step"><?php _e('Сontinue', 'rooom'); ?></button>
								<input class="form__submit disabled" type="submit" value="<?php _e('Send', 'rooom'); ?>">
							</div>
							<div class="line"></div>
							<div class="text">1/2</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
			<?php if( is_singular('project') ) { ?>
				<div class="nav__block" data-aos="fade-left" data-aos-duration="600"><a href="<?php echo $archive_link; ?>"><?php _e('To all projects', 'rooom'); ?></a></div>
			<?php } ?>
			</div>
			<div class="col-md-6">
				<div id="to__top" class="btn btn__dark top__icon float-right" data-aos="fade-up" data-aos-duration="600"><span><?php _e('To top', 'rooom'); ?></span></div>
			</div>
		</div>
	</div>
</section>