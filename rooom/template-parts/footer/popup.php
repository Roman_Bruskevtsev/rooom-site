<?php
$form = get_field('popup', 'option'); 
?>
<div class="popup__wrapper"></div>
<div class="popup">
	<span class="close"></span>
	<?php if( $form['title'] ) { ?>
	<div class="title text-center">
		<h3 class="h6"><?php echo $form['title']; ?></h3>
	</div>
	<?php } ?>
	<form onsubmit="return false;" class="text-center">
		<?php wp_nonce_field('security', 'form-security_'.wp_unique_id()); ?>
		<div class="field__group inline">
			<label class="text-left">
				<span class="title"><?php _e('Your name', 'rooom'); ?></span>
				<input class="form__field" required type="text" name="name" placeholder="<?php _e('Name', 'rooom'); ?>">
			</label>
			<label class="text-left">
				<span class="title"><?php _e('Phone', 'rooom'); ?></span>
				<input class="form__field" required type="tel" name="phone" placeholder="<?php _e('096 1234 123', 'rooom'); ?>">
			</label>
		</div>
		<div class="submit__block">
			<button type="submit" class="btn btn__submit"><span><?php _e('Send', 'rooom'); ?></span></button>
		</div>
	</form>
</div>