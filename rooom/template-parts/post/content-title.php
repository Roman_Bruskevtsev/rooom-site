<?php 
$categories = get_the_category();
?>
<div class="post__title">
	<?php if( $categories ){
		echo '<div class="categories">';
		foreach ( $categories as $category ) {
			$color = get_field( 'category_color', 'category_'.$category->term_id ) ? get_field( 'category_color', 'category_'.$category->term_id ) : '#2C2C2D';
			
			echo '<a href="'.get_tag_link($category->term_id).'" style="color: '.$color.'; border-color: '.$color.'">'.$category->name.'</a>';
		}
		echo '</div>';
	} ?>
	<h1 class="h4"><?php the_title(); ?></h1>
</div>