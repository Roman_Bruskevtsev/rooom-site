<section class="two__images">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-8">
				<div class="text" data-aos="fade-up" data-aos-duration="600"><?php the_sub_field('text'); ?></div>
			</div>
		</div>
		<div class="row justify-content-center">
			<?php if( get_sub_field('image_1') ) { ?>
			<div class="col-lg-4">
				<div class="image" data-aos="fade-right" data-aos-duration="600">
					<img src="<?php echo get_sub_field('image_1')['url']; ?>" alt="<?php echo get_sub_field('image_1')['title']; ?>">
				</div>
			</div>
			<?php } ?>
			<?php if( get_sub_field('image_2') ) { ?>
			<div class="col-lg-4">
				<div class="image" data-aos="fade-left" data-aos-duration="600">
					<img src="<?php echo get_sub_field('image_2')['url']; ?>" alt="<?php echo get_sub_field('image_2')['title']; ?>">
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>