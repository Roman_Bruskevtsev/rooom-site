<section class="images__slider__section">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-8">
				<div class="text" data-aos="fade-up" data-aos-duration="600"><?php the_sub_field('text'); ?></div>
			</div>
		</div>
		<?php if( get_sub_field('images') ) { ?>
		<div class="row justify-content-center">
			<div class="col-lg-8">
				<div class="post__slider swiper-container" data-aos="fade-up" data-aos-duration="600">
					<div class="swiper-wrapper">
					<?php foreach ( get_sub_field('images') as $image ) { ?>
						<div class="swiper-slide">
							<div class="image">
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
							</div>
						</div>
					<?php } ?>
					</div>
				</div>
				<div class="swiper-pagination"></div>
			    <div class="swiper-button-prev cursor__hover"></div>
			    <div class="swiper-button-next cursor__hover"></div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>