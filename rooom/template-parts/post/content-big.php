<?php 
$categories = get_the_category();
$background = get_the_post_thumbnail_url() ? ' style="background-image: url('.get_the_post_thumbnail_url().')"' : '';
?>
<div class="post__block big" data-aos="fade-up" data-aos-duration="600">
	<div class="image"<?php echo $background; ?>></div>
	<?php if( $categories ){
		echo '<div class="categories">';
		foreach ( $categories as $category ) {
			$color = get_field( 'category_color', 'category_'.$category->term_id ) ? get_field( 'category_color', 'category_'.$category->term_id ) : '#2C2C2D';
			
			echo '<a href="'.get_tag_link($category->term_id).'" style="color: '.$color.'; border-color: '.$color.'">'.$category->name.'</a>';
		}
		echo '</div>';
	} ?>
	<div class="content">
		<h2 class="h5"><?php the_title(); ?></h2>
		<a href="<?php the_permalink(); ?>"><?php _e('Read all', 'rooom'); ?></a>
	</div>
</div>