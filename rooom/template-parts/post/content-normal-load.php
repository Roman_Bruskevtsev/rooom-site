<?php 
$categories = get_the_category();
$background = get_the_post_thumbnail_url() ? ' style="background-image: url('.get_the_post_thumbnail_url().')"' : '';
?>
<div class="col-lg-4">
	<a class="post__block normal" href="<?php the_permalink(); ?>" data-aos="fade-up" data-aos-duration="600">
		<div class="thumbnail">
			<div class="image"<?php echo $background; ?>></div>
			<?php if( $categories ){
				echo '<div class="categories">';
				foreach ( $categories as $category ) {
					$color = get_field( 'category_color', 'category_'.$category->term_id ) ? get_field( 'category_color', 'category_'.$category->term_id ) : '#2C2C2D';
					
					echo '<div class="category" style="color: '.$color.'; border-color: '.$color.'">'.$category->name.'</div>';
				}
				echo '</div>';
			} ?>
		</div>
		<h2 class="h6"><?php the_title(); ?></h2>
	</a>
</div>