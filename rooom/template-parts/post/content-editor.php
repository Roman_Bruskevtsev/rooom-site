<section class="content">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-8">
				<div class="text" data-aos="fade-up" data-aos-duration="600"><?php the_sub_field('text'); ?></div>
			</div>
		</div>
	</div>
</section>