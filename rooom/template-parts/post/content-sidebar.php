<?php $args = array(
	'post_type'				=> 'post',
	'posts_per_page' 		=> 5,
	'post_status'			=> 'publish',
	'ignore_sticky_posts'	=> true
);

$query = new WP_Query( $args ); 

if ( $query->have_posts() ) { ?>
<div class="post__sidebar" data-aos="fade-left" data-aos-duration="600">
	<h6><b><?php _e('The latest in our blog', 'rooom'); ?></b></h6>
	<?php while ( $query->have_posts() ) { $query->the_post(); 
		$background = get_the_post_thumbnail_url() ? ' style="background-image: url('.get_the_post_thumbnail_url().')"' : '';
		$title = strlen( get_the_title() ) > 110 ? substr( get_the_title(), 0, 110 ).'...' : get_the_title(); ?>
		<div class="post__row">
			<a href="<?php the_permalink(); ?>" class="thumbnail"<?php echo $background; ?>></a>
			<div class="content">
				<h6><?php echo $title; ?></h6>
				<a href="<?php the_permalink(); ?>"><?php _e('Read all', 'rooom'); ?></a>
			</div>
		</div>
	<?php } ?>
</div>
<?php } else {
	
}

wp_reset_postdata(); ?>