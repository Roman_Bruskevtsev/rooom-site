<section class="text__image__section">
	<div class="container-fluid">
		<?php if( get_sub_field('text') ) { ?>
		<div class="row">
			<div class="col-lg-6">
				<div class="text" data-aos="fade-up" data-aos-duration="600"><?php the_sub_field('text'); ?></div>
			</div>
		</div>
		<?php } ?>
		<div class="row">
			<?php 
			$image = get_sub_field('image_left'); 
			if( $image ) { ?>
			<div class="col-lg-6">
				<div class="image" data-aos="fade-right" data-aos-duration="600">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
				</div>
			</div>
			<?php } ?>
			<?php 
			$image = get_sub_field('image_right'); 
			if( $image ) { ?>
			<div class="col-lg-6">
				<div class="image" data-aos="fade-left" data-aos-duration="600">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>