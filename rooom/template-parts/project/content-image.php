<?php 
$image = get_sub_field('image'); 
if( $image ) { ?>
<section class="image__section" data-aos="fade-up" data-aos-duration="600">
	<div class="container-fluid">
		<div class="row">
			<div class="col">
				<img src="<?php echo $image['url']; ?>" title="<?php echo $image['title']; ?>">
			</div>
		</div>
	</div>
</section>
<?php } ?>