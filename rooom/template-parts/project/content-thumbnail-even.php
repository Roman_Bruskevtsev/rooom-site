<?php 
$thumbnail = get_field('thumbnails')['tall'];
?>
<div class="row justify-content-start">
	<?php if( $thumbnail ) { ?>
	<div class="col-lg-6 col-xl-6">
		<a class="project__thumbnail tall" href="<?php the_permalink(); ?>">
			<img src="<?php echo $thumbnail['url']; ?>" alt="<?php the_field('short_title'); ?>">
		</a>
	</div>
	<?php } ?>
	<div class="col-lg-6 col-xl-3">
		<a class="project__title tall" href="<?php the_permalink(); ?>">
			<span class="location" data-aos="fade-right" data-aos-duration="500"><?php the_field('location'); ?></span>
			<h5 data-aos="fade-right" data-aos-duration="500" data-aos-delay="250"><?php the_field('short_title'); ?></h5>
			<div class="line" data-aos="fade-right" data-aos-duration="500" data-aos-delay="350"></div>
		</a>
	</div>
	
</div>