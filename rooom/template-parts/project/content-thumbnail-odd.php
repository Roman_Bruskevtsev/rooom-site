<?php 
$thumbnail = get_field('thumbnails')['wide'];
?>
<div class="row justify-content-end">
	<div class="col-lg-4 col-xl-3">
		<a class="project__title wide" href="<?php the_permalink(); ?>">
			<span class="location" data-aos="fade-left" data-aos-duration="500"><?php the_field('location'); ?></span>
			<h5 data-aos="fade-left" data-aos-duration="500" data-aos-delay="250"><?php the_field('short_title'); ?></h5>
			<div class="line" data-aos="fade-left" data-aos-duration="500" data-aos-delay="350"></div>
		</a>
	</div>
	<?php if( $thumbnail ) { ?>
	<div class="col-lg-8 col-xl-8">
		<a class="project__thumbnail wide" href="<?php the_permalink(); ?>">
			<img src="<?php echo $thumbnail['url']; ?>" alt="<?php the_field('short_title'); ?>">
		</a>
	</div>
	<?php } ?>
</div>
