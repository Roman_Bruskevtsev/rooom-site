<section class="project__details">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-6 col-xl-5">
				<?php if( get_sub_field('text') ) { ?><div class="text" data-aos="fade-up" data-aos-duration="600"><?php the_sub_field('text'); ?></div><?php } ?>
			</div>
			<div class="col-lg-1"></div>
			<div class="col-lg-5 col-xl-4">
				<?php 
				$details = get_sub_field('details'); 
				if( $details ) { ?>
				<div class="details" data-aos="fade-left" data-aos-duration="600">
					<?php if( $details['square'] ) { ?>
					<div class="detail__row square">
						<div class="label"><?php _e('Square:', 'rooom'); ?></div>
						<div class="value"><?php echo $details['square']; ?>м<sup>2</sup></div>
					</div>
					<?php }
					if( $details['style'] ) { ?>
					<div class="detail__row style">
						<div class="label"><?php _e('Style:', 'rooom'); ?></div>
						<div class="value"><?php echo $details['style']; ?></div>
					</div>
					<?php }
					if( $details['type'] ) { ?>
					<div class="detail__row type">
						<div class="label"><?php _e('Object type:', 'rooom'); ?></div>
						<div class="value"><?php echo $details['type']; ?></div>
					</div>
				<?php } ?>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>