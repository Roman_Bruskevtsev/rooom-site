<section class="third__image__section">
	<div class="container-fluid">
		<div class="row">
			<?php 
			$image_1 = get_sub_field('image_1'); 
			if( $image_1 ) { ?>
			<div class="col-lg-4">
				<div class="image" data-aos="fade-up" data-aos-duration="600">
					<img src="<?php echo $image_1['url']; ?>" alt="<?php echo $image_1['title']; ?>">
				</div>
			</div>
			<?php } ?>
			<?php 
			$image_2 = get_sub_field('image_2'); 
			if( $image_2 ) { ?>
			<div class="col-lg-4">
				<div class="image" data-aos="fade-up" data-aos-duration="600">
					<img src="<?php echo $image_2['url']; ?>" alt="<?php echo $image_2['title']; ?>">
				</div>
			</div>
			<?php } ?>
			<?php 
			$image_3 = get_sub_field('image_3'); 
			if( $image_3 ) { ?>
			<div class="col-lg-4">
				<div class="image" data-aos="fade-up" data-aos-duration="600">
					<img src="<?php echo $image_3['url']; ?>" alt="<?php echo $image_3['title']; ?>">
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>