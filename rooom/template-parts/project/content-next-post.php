<?php 
$next_post = get_next_post();
if( $next_post ) { 
	$background = get_the_post_thumbnail_url($next_post->ID) ? ' style="background-image: url('.get_the_post_thumbnail_url($next_post->ID, 'full').')"' : '';
	?>
<section class="next__post__section"<?php echo $background; ?>>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="text text-center">
					<a href="<?php echo get_permalink($next_post->ID); ?>"><?php _e('Next project', 'rooom'); ?></a>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>