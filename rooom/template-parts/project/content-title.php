<?php 
$archive_link = get_post_type_archive_link('project'); 
?>
<section class="project__title">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-2">
				<div class="nav__block" data-aos="fade-right" data-aos-duration="600"><a href="<?php echo $archive_link; ?>"><?php _e('To all projects', 'rooom'); ?></a></div>
			</div>
			<div class="col-lg-7">
				<div class="title" data-aos="fade-left" data-aos-duration="600">
					<?php if( get_field( 'location' ) ) { ?><div class="location"><?php the_field('location'); ?></div><?php } ?>
					<h1 class="h5"><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid nopadding">
		<div class="row">
			<div class="col-lg-12">
				<div class="banner" data-aos="fade-up" data-aos-duration="600">
					<?php echo get_the_post_thumbnail( get_the_ID(), 'full'); ?>
				</div>
			</div>
		</div>
	</div>
</section>