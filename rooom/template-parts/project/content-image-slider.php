<section class="images__section">
	<div class="container-fluid nopadding">
		<div class="row">
			<div class="col-lg-2"></div>
			<div class="col-lg-2">
				<?php if( get_sub_field('title') ) { ?>
				<div class="title" data-aos="fade-right" data-aos-duration="600">
					<h3><?php the_sub_field('title'); ?></h3>
				</div>
				<?php } ?>
			</div>
			<div class="col-lg-1">
			</div>
			<div class="col-lg-6">
				<?php $images = get_sub_field('images'); 
				if( $images ) { ?>
				<div class="images__slider swiper-container" data-aos="fade-left" data-aos-duration="600">
					<div class="swiper-wrapper">
					<?php 
					$total_count = count($images) < 10 ? '0'.count($images) : count($images);
					$i = 1;
					foreach ( $images as $image ) { 
						$amount = $i < 10 ? '0'.$i : $i;
						$background = ' style="background-image: url('.$image['url'].')"'; ?>
						<div class="swiper-slide cursor__scroll"<?php echo $background; ?>>
							<div class="count"><?php echo $amount; ?>/<?php echo $total_count; ?></div>
						</div>
					<?php $i++; } ?>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-2"></div>
			<div class="col-lg-2"></div>
		</div>
	</div>
</section>