<?php
/**
 *
 * @package WordPress
 * @subpackage Rooom
 * @since 1.0
 * @version 1.0
 */
get_header(); 

while ( have_posts() ) : the_post(); 

	get_template_part( 'template-parts/project/content', 'title' );

	if( have_rows('content') ):

		while ( have_rows('content') ) : the_row();

			if( get_row_layout() == 'project_details' ) :
				get_template_part( 'template-parts/project/content', 'project-details' );
			elseif( get_row_layout() == 'image' ) :
				get_template_part( 'template-parts/project/content', 'image' );
			elseif( get_row_layout() == 'image_slider' ) :
				get_template_part( 'template-parts/project/content', 'image-slider' );
			elseif( get_row_layout() == 'text_image_image_section' ) :
				get_template_part( 'template-parts/project/content', 'text-image-image-section' );
			elseif( get_row_layout() == '13_image_13_image_13_image_block' ) :
				get_template_part( 'template-parts/project/content', '13-image-section' );
			elseif( get_row_layout() == 'half_small_image_section' ) :
				get_template_part( 'template-parts/project/content', 'half-small-image-section' );
				
			endif;

		endwhile;

	endif;

	get_template_part( 'template-parts/footer/price-form' );

	get_template_part( 'template-parts/project/content', 'next-post' );
	
endwhile; 

get_footer();