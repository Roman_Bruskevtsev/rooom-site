<?php
/**
 *
 * @package WordPress
 * @subpackage Rooom
 * @since 1.0
 * @version 1.0
 */
get_header(); 

while ( have_posts() ) : the_post(); ?>
	
	<section class="post__info">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-lg-8">
					<?php 
					get_template_part( 'template-parts/breadcrumbs' ); 
					get_template_part( 'template-parts/post/content', 'title' ); 
					?>
				</div>
			</div>
		</div>
	</section>
	<?php if( have_rows('content') ):

		while ( have_rows('content') ) : the_row();

			if( get_row_layout() == 'content_editor' ) :
				get_template_part( 'template-parts/post/content', 'editor' );
			elseif( get_row_layout() == 'image_slider' ) :
				get_template_part( 'template-parts/post/content', 'image-slider' );
			elseif( get_row_layout() == 'two_half_images' ) :
				get_template_part( 'template-parts/post/content', 'two-images' );
			elseif( get_row_layout() == 'images_grid' ) :
				get_template_part( 'template-parts/post/content', 'images-grid' );
			endif;

		endwhile;

	endif; ?>
</div>
	
<?php endwhile; 

get_template_part( 'template-parts/footer/price-form' );

get_footer();