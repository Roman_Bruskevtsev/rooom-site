<?php
/**
 *
 * @package WordPress
 * @subpackage Rooom
 * @since 1.0
 * @version 1.0
 */
/*Theme settings*/
require get_template_directory() . '/inc/classes/theme_settings.php';
require get_template_directory() . '/inc/classes/forms.php';
require get_template_directory() . '/inc/classes/rooom.php';