<?php
/**
 *
 * @package WordPress
 * @subpackage Rooom
 * @since 1.0
 * @version 1.0
 */

get_header(); 

if( have_rows('content') ):

	while ( have_rows('content') ) : the_row();

		if( get_row_layout() == 'projects_section' ) :
			get_template_part( 'template-parts/page/content', 'projects-section' );
		elseif( get_row_layout() == 'projects_blocks_section' ) :
			get_template_part( 'template-parts/page/content', 'projects-blocks-section' );
		elseif( get_row_layout() == 'testimonals_section' ) :
			get_template_part( 'template-parts/page/content', 'testimonals-section' );
		elseif( get_row_layout() == 'contact_section' ) :
			get_template_part( 'template-parts/page/content', 'contact-section' );
		endif;

	endwhile;

endif;

get_footer();