'use strict';

class GeneralClass{

    constructor(){
        this.init();
    }

    init(){
        this.popup();
        this.lettersSection();
        this.projectsSection();
        this.projectsBlock();
        this.testimonialsSection();
        this.projectsArchive();
        this.imagesSlider();
        this.loadPosts();
        this.postImagesSlider();
        this.priceForm();
        this.contactForm();
        this.workSlider();
        this.partnersSection();
        this.rooomSection();

        this.documentReady();
        this.windowLoad();
        this.windowScroll();
    }

    documentReady(){
        jQuery(document).ready(function($) {
            
        }); 

        document.addEventListener('DOMContentLoaded', function(){
            document.getElementById('show__menu').addEventListener('click', function(){
                let menuWrapper = document.querySelector('.menu__wrapper'),
                    header = document.querySelector('header');

                this.classList.toggle('show__menu');
                menuWrapper.classList.toggle('show__menu');
                header.classList.toggle('show__menu');
            });

            if( document.getElementById('to__top') ){
                document.getElementById('to__top').addEventListener('click', function(){
                    window.scrollTo({
                        top: 0,
                        behavior: 'smooth'
                    });
                });
            }

            if( document.querySelector('.page__scroll') ){
                let bar = document.querySelector('.page__scroll span'),
                    windowHeight = window.innerHeight,
                    pageHeight = document.body.scrollHeight - windowHeight,
                    scrollPosition = window.scrollY,
                    percent = ( 100 * scrollPosition ) / pageHeight,
                    left = percent > 100 ? 100 : percent;

                bar.style.left = left + '%';
                window.addEventListener('scroll', function() {
                    scrollPosition = window.scrollY;

                    percent = ( 100 * scrollPosition ) / pageHeight;
                    left = percent > 98 ? 100 : percent;

                    bar.style.left = left + '%';
                });
            }
        });
    }

    windowLoad(){
        document.addEventListener('DOMContentLoaded', function(event) {
            rooom.preloader();
            rooom.mouseMove();
            rooom.googleMap();
        });

        jQuery(document).ready(function($) {
            AOS.init();
            $('.paroller').paroller(); 
        });
    }

    windowScroll(){
        let header = document.querySelector('header'),
            menuWrapper = document.querySelector('.menu__wrapper'),
            scrollHeight;
        window.addEventListener('scroll', function() {
            scrollHeight = window.pageYOffset;

            if( scrollHeight > 100 ){
                header.classList.add('show__bar');
                menuWrapper.classList.add('scroll');
            } else {
                header.classList.remove('show__bar');
                menuWrapper.classList.remove('scroll');
            }
        });
    }

    preloader(){
        let step = 50,
            i = 0,
            wrapper = document.querySelector('.preloader__wrapper'),
            layout = wrapper.querySelector('.layout');
        function load(){
            setTimeout(function () {
                i+=5;
                if( i <= 100) {
                    layout.style.left = i + '%';
                    load();
                } else {
                    setTimeout(function () { 
                        wrapper.classList.add('load');
                    }, 500);
                }

            }, step);
        }
        load();
    }

    mouseMove(){
        let body = document.querySelector('body'),
            cursor = body.querySelector('.cursor'),
            circle = cursor.querySelector('.circle'),
            dot = cursor.querySelector('.dot'),
            hoverElementArray = [
                'A',
                'BUTTON'
            ],
            mouseOverElement, mouseOverElementClasslist;

        body.addEventListener('mousemove', e => {
            mouseOverElement = e.target.tagName;
            mouseOverElementClasslist = e.target.classList;
            
            if( hoverElementArray.includes(mouseOverElement) || mouseOverElementClasslist.contains('cursor__hover') ){
                cursor.classList.add('hover');
                cursor.classList.remove('scroll');
                circle.setAttribute('style', 'transform: matrix(1, 0, 0, 1, '+ e.pageX + ',' + e.pageY + ') scale(0.4)');
            } else if( mouseOverElementClasslist.contains('cursor__scroll') ){
                cursor.classList.add('scroll');
                cursor.classList.remove('hover');
                circle.setAttribute('style', 'transform: matrix(1, 0, 0, 1, '+ e.pageX + ',' + e.pageY + ') scale(0.5)');
            } else {
                cursor.classList.remove('hover');
                cursor.classList.remove('scroll');
                circle.setAttribute('style', 'transform: matrix(1, 0, 0, 1, '+ e.pageX + ',' + e.pageY + ') scale(1)');
            }
            
            dot.setAttribute('style', 'transform: matrix(1, 0, 0, 1, '+ e.pageX + ',' + e.pageY + ')');
            // circle.setAttribute('style', 'transform: matrix(1, 0, 0, 1, '+ e.pageX + ',' + e.pageY + ')');
        });
    }

    popup(){
        let showPopups = document.querySelectorAll('.show__popup'),
            popup = document.querySelector('.popup'),
            close = popup.querySelector('.close'),
            popupWrapper = document.querySelector('.popup__wrapper');

        showPopups.forEach(function(showPopup){
            showPopup.addEventListener('click', function(){
                popup.classList.add('show');
                popupWrapper.classList.add('show');
            });
        });

        popupWrapper.addEventListener('click', function(){
            popup.classList.remove('show');
            popupWrapper.classList.remove('show');
        });

        close.addEventListener('click', function(){
            popup.classList.remove('show');
            popupWrapper.classList.remove('show');
        });
    }

    lettersSection(){
        let section = document.querySelector('.projects__section');

        if( section ){
            let words = section.querySelectorAll('.title li'),
                currentWord = 0;

            function changeWord() {
                words.forEach(function(word){
                    word.classList.remove('active');
                    word.classList.remove('prev');
                });
                if( currentWord != 0 ) words[currentWord - 1].classList.add('prev');
                words[currentWord].classList.add('active');

                if( currentWord == words.length - 1){
                    currentWord = 0;
                } else {
                    currentWord++;
                }
            }

            changeWord();

            setInterval(changeWord, 4000);
        }
    }

    projectsSection(){
        let slider = document.querySelector('.projects__slider');

        if(slider){
            var swiper = new Swiper('.projects__slider', {
                    slidesPerView: 1,
                    spaceBetween: 0,
                    speed: 1500,
                    effect: 'fade',
                    autoplay: {
                        delay: 3000,
                    }
                });

            // window.addEventListener('scroll', function() {
            //     domRect = slider.getBoundingClientRect();
            //     scrollToTop = domRect.y;
            //     scrollEvent = ( scrollToTop < 200 && scrollToTop > -450 ) ? true : false
            // });

            // slider.onwheel = function(e){
            //     scroll = e.deltaY;
            //     delta = delta + scroll;
                
            //     if( scroll < 0 && currentSlide != 0 && scrollEvent ){
            //         if( delta <= -500 ){
            //             delta = 0;
            //             swiper.slidePrev();
            //         }
            //         delta = delta - 100;
            //         e.preventDefault();
            //     } else if ( scroll > 0 && currentSlide != slides - 1 && scrollEvent ) {
            //         if( delta >= 500 ){
            //             delta = 0;
            //             swiper.slideNext();
            //         }
            //         delta = delta + 100;
            //         e.preventDefault();
            //     }
            // }

            // swiper.on('slideChange', function () {
            //     currentSlide = swiper.activeIndex;
            // });
        }
    }

    projectsBlock(){
        let blocks = document.querySelectorAll('.project__thumbnail');
        if( blocks ){
            let windowHeight,
                scrollPosition,
                blockHeight,
                blockWindowTop,
                centerTopBlock,
                centerBottomBlock,
                scrollCenter;
            window.addEventListener('scroll', function() {
                windowHeight = window.innerHeight;
                scrollPosition = window.scrollY;
                blocks.forEach(function(block){
                    blockHeight        = block.getBoundingClientRect().height;
                    blockWindowTop     = block.getBoundingClientRect().top;
                    centerTopBlock     = blockWindowTop + blockHeight - 800;
                    centerBottomBlock  = blockWindowTop + blockHeight + 600;
                    scrollCenter       = scrollPosition + windowHeight / 2;

                    if( ( centerTopBlock < windowHeight && centerBottomBlock > windowHeight ) ){
                        block.classList.add('show__image');
                    }

                    if( centerBottomBlock < windowHeight || centerTopBlock > windowHeight ){
                        block.classList.remove('show__image');
                    }
                });
            });
        }
    }

    testimonialsSection(){
        let slider = document.querySelector('.testimonials__slider');
        if( slider ){
            var swiper = new Swiper('.testimonials__slider', {
                    slidesPerView: 1,
                    spaceBetween: 0,
                    speed: 1500,
                    breakpoints: {
                        1200: {
                            slidesPerView: 2
                        }
                    }
                }),
                currentSlide = 0,
                slides = swiper.slides.length,
                scroll, delta = 0,
                domRect, scrollToTop, scrollEvent = false;

            window.addEventListener('scroll', function() {
                domRect = slider.getBoundingClientRect();
                scrollToTop = domRect.y;
                scrollEvent = ( scrollToTop < 400 && scrollToTop > -450 ) ? true : false;
            });

            slider.onwheel = function(e){
                scroll = e.deltaY;
                delta = delta + scroll;
                if( scroll < 0 && currentSlide != 0 && scrollEvent ){
                    if( delta <= -500 ){
                        delta = 0;
                        swiper.slidePrev();
                    }
                    delta = delta - 100;
                    e.preventDefault();
                } else if ( scroll > 0 && currentSlide != slides - 1 && scrollEvent ) {
                    if( delta >= 500 ){
                        delta = 0;
                        swiper.slideNext();
                    }
                    delta = delta + 100;
                    e.preventDefault();
                }
            }

            swiper.on('slideChange', function () {
                currentSlide = swiper.activeIndex;
            });
        }
    }

    googleMap(){
        let map = document.querySelector('.map');
        if( map ){
            let address = map.dataset.str,
                latitude = map.dataset.latitude,
                longitude = map.dataset.longitude,
                zoom = parseInt(map.dataset.zoom),
                markerIcon = map.dataset.marker,
                style = [
                            {
                                "featureType": "administrative",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "saturation": "-100"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative.province",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "landscape",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "saturation": -100
                                    },
                                    {
                                        "lightness": 65
                                    },
                                    {
                                        "visibility": "on"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "saturation": -100
                                    },
                                    {
                                        "lightness": "50"
                                    },
                                    {
                                        "visibility": "simplified"
                                    }
                                ]
                            },
                            {
                                "featureType": "road",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "saturation": "-100"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "visibility": "simplified"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.arterial",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "lightness": "30"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.local",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "lightness": "40"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "saturation": -100
                                    },
                                    {
                                        "visibility": "simplified"
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "hue": "#ffff00"
                                    },
                                    {
                                        "lightness": -25
                                    },
                                    {
                                        "saturation": -97
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "labels",
                                "stylers": [
                                    {
                                        "lightness": -25
                                    },
                                    {
                                        "saturation": -100
                                    }
                                ]
                            }
                        ],
                myLatlng = new google.maps.LatLng(latitude, longitude),
                styledMap = new google.maps.StyledMapType(style, { name: "Styled Map" }),
                mapOptions = {
                    zoom: zoom,
                    disableDefaultUI: false,
                    scrollwheel: false,
                    zoomControl: true,
                    streetViewControl: true,
                    center: myLatlng
                },
                googleMap = new google.maps.Map(document.getElementById('google__map'), mapOptions),
                mark_location = new google.maps.LatLng(latitude, longitude);

                googleMap.mapTypes.set('map_style', styledMap);
                googleMap.setMapTypeId('map_style');

                let marker = new google.maps.Marker({
                    position: mark_location,
                    map: googleMap,
                    icon: markerIcon
                });

                marker.setMap(googleMap);

                let infoWindow = new google.maps.InfoWindow({
                    content: address
                });

                google.maps.event.addListener(marker, 'click', function() {
                    infoWindow.open(googleMap, marker);
                });
        }
    }

    projectsArchive(){
        let slider = document.querySelector('.archive__slider'),
            grid = document.querySelector('.archive__grid');

        if( slider ){
            let slidesBtn = document.querySelector('.view__bar .slider'),
                gridBtn = document.querySelector('.view__bar .grid'),
                swiper = new Swiper('.archive__slider', {
                    slidesPerView: 1,
                    spaceBetween: 0,
                    speed: 1500,
                    spaceBetween: 195,
                }),
                currentSlideLabel = document.querySelector('.projects__count .current'),
                currentSlide = 0,
                slides = swiper.slides.length,
                scroll, delta = 0;

            slider.onwheel = function(e){
                scroll = e.deltaY;
                delta = delta + scroll;
                
                if( scroll < 0 && currentSlide != 0 ){
                    if( delta <= -400 ){
                        delta = 0;
                        swiper.slidePrev();
                    }
                    delta = delta - 100;
                    e.preventDefault();
                } else if ( scroll > 0 && currentSlide != slides - 1 ) {
                    if( delta >= 400 ){
                        delta = 0;
                        swiper.slideNext();
                    }
                    delta = delta + 100;
                    e.preventDefault();
                }
            }

            swiper.on('slideChange', function () {
                currentSlide = swiper.activeIndex;
                currentSlideLabel.textContent = currentSlide + 1 < 10 ? '0' + (currentSlide + 1) : currentSlide + 1;
            });

            slidesBtn.addEventListener('click', function(){
                this.classList.add('active');
                gridBtn.classList.remove('active');
                slider.classList.add('active');
                grid.classList.remove('active');
            })
            gridBtn.addEventListener('click', function(){
                this.classList.add('active');
                slidesBtn.classList.remove('active');
                grid.classList.add('active');
                slider.classList.remove('active');
            })
        }
    }

    imagesSlider(){
        let slider = document.querySelector('.images__slider');

        if( slider ){
            let swiper = new Swiper('.images__slider', {
                    slidesPerView: 1,
                    spaceBetween: 0,
                    speed: 1500,
                    spaceBetween: 90,
                }),
                currentSlide = 0,
                slides = swiper.slides.length,
                scroll, delta = 0;

            slider.onwheel = function(e){
                scroll = e.deltaY;
                delta = delta + scroll;
                
                if( scroll < 0 && currentSlide != 0 ){
                    if( delta <= -400 ){
                        delta = 0;
                        swiper.slidePrev();
                    }
                    delta = delta - 100;
                    e.preventDefault();
                } else if ( scroll > 0 && currentSlide != slides - 1 ) {
                    if( delta >= 400 ){
                        delta = 0;
                        swiper.slideNext();
                    }
                    delta = delta + 100;
                    e.preventDefault();
                }
            }

            swiper.on('slideChange', function () {
                currentSlide = swiper.activeIndex;
            });
        }
    }

    loadPosts(){
        let loadLine = document.querySelector('.blog__section .load__posts');

        if( loadLine ){
            let wrapper = document.querySelector('.blog__section .row.posts__row'),
                pages = parseInt(loadLine.dataset.pages),
                category = loadLine.dataset.category,
                currentPage = parseInt(loadLine.dataset.current),
                load = false,
                xhr = new XMLHttpRequest();

            loadLine.addEventListener('click', function(){
                if( pages > currentPage && !load ){
                    loadLine.classList.add('load');
                    loadLine.dataset.current = currentPage++;
                    load = true;
                    xhr.open('POST', ajaxurl, true)
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;');
                    xhr.onload = function () {
                        loadLine.classList.remove('load');
                        let res =  this.response;
                        wrapper.insertAdjacentHTML('beforeEnd', res);
                        load = false;
                        
                        if( pages <= parseInt(loadLine.dataset.current) + 1 ) loadLine.remove();
                        AOS.init();
                    };
                    xhr.onerror = function() {
                        console.log('connection error');
                    };
                    xhr.send('action=load_posts' +
                             '&category=' + category +
                             '&page=' + currentPage);
                }
            });
        }
    }

    postImagesSlider(){
        let slider = document.querySelector('.post__slider');

        if( slider ){
            let swiper = new Swiper('.post__slider', {
                slidesPerView: 1,
                speed: 1000,
                spaceBetween: 30,
                pagination: {
                    el: '.swiper-pagination',
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                breakpoints: {
                    991: {
                        slidesPerView: 2
                    }
                }
            });
        }
    }

    workSlider(){
        let slider = document.querySelector('.works__slider');

        if( slider ){
            let swiper = new Swiper('.works__slider', {
                slidesPerView: 1,
                    spaceBetween: 0,
                    speed: 1500,
                    spaceBetween: 80,
                    breakpoints: {
                        991: {
                            width: 1300
                        }
                    }
                }),
                wrapper = slider.closest('.what__do__section'),
                currentSlide = 0,
                slidePossibility = false,
                slides = swiper.slides.length,
                scroll, sliderScroll, sliderHeight, delta = 0;
            window.addEventListener('scroll', function() {
                sliderScroll = slider.getBoundingClientRect().top;
                sliderHeight = slider.getBoundingClientRect().height;
                
                if( sliderScroll < 350 && sliderScroll > -350){
                    slidePossibility = true;
                } else {
                    slidePossibility = false;
                }
            });
            wrapper.onwheel = function(e){
                scroll = e.deltaY;
                delta = delta + scroll;
                
                if( scroll < 0 && currentSlide != 0 && slidePossibility ){
                    if( delta <= -400 ){
                        delta = 0;
                        swiper.slidePrev();
                    }
                    delta = delta - 100;
                    e.preventDefault();
                } else if ( scroll > 0 && currentSlide != slides - 1 && slidePossibility ) {
                    if( delta >= 400 ){
                        delta = 0;
                        swiper.slideNext();
                    }
                    delta = delta + 100;
                    e.preventDefault();
                }
            }

            swiper.on('slideChange', function () {
                currentSlide = swiper.activeIndex;
            });
        }
    }

    priceForm(){
        let form = document.querySelector('.price__form__section .form__block form');
        if( form ) {
            let step1 = form.querySelector('.steps .step__1'),
                step2 = form.querySelector('.steps .step__2'),
                stepsNav = form.querySelector('.steps__nav'),
                prevStep = stepsNav.querySelector('.prev__step'),
                nextStep = stepsNav.querySelector('.next__step'),
                types = form.querySelectorAll('input[name="design-type"]'),
                formFields = form.querySelectorAll('input.form__field'),
                submitField = form.querySelector('input[type="submit"]'),
                value, type, fieldName, name, phone, email, label;

            types.forEach(function(type){
                type.addEventListener('change', function(field){
                    type = field.target.value;
                    
                    nextStep.classList.remove('disabled');
                });
                type.addEventListener('click', function(field){
                    types.forEach(function(type){
                        type.closest('label').classList.remove('checked');
                    });
                    field.target.closest('label').classList.add('checked');
                    
                    // label = type.closest('label');
                    // label.classList.add('checked');
                });
            })

            formFields.forEach(function(field){
                field.addEventListener('keyup', function(formField){
                    value = formField.target.value;
                    fieldName = formField.target.getAttribute('name');
                    
                    switch(fieldName){
                        case 'name':
                            name = value;
                            break;
                        case 'phone':
                            phone = value;
                            break;
                        case 'email':
                            email = value;
                            break;
                    }

                    if(name && phone && email){
                        submitField.classList.remove('disabled');
                    } else {
                        submitField.classList.add('disabled');
                    }
                });
            })

            form.addEventListener('submit', function(){
                let formData           = new FormData(),
                    fields             = form.querySelectorAll('.form__field'),
                    radioFields        = form.querySelectorAll('.form__radio'),
                    fieldsError        = form.querySelectorAll('.field__error'),
                    formResponse       = form.querySelector('.form__response'),
                    securityField      = form.querySelector('input[name*="form-security"]'),
                    xhr                = new XMLHttpRequest(),
                    filesParam         = '',
                    fieldsArray        = {},
                    fieldArray, fieldGroup;

                    submitField.classList.add('load');

                    if(fieldsError.length){
                        fieldsError.forEach(function(field){
                            field.remove();
                        });
                    }
                    if(formResponse) formResponse.remove();

                    fields.forEach(function(field, e){
                        if( field.getAttribute('type') == 'radio' ){
                            if( field.checked ) {
                                fieldArray = {
                                    name: '',
                                    value: '',
                                    required: false
                                };
                                fieldGroup = field.closest('label');
                                fieldArray.required = (field.required) ? true : false;
                                fieldArray.value = field.value;
                                fieldArray.name = field.getAttribute('name');
                                field.classList.remove('error');
                                if( fieldGroup ) fieldGroup.classList.remove('error');
                                fieldsArray[e] = fieldArray;
                            }
                        } else {
                            fieldArray = {
                                name: '',
                                value: '',
                                required: false
                            };
                            fieldGroup = field.closest('label');
                            fieldArray.required = (field.required) ? true : false;
                            fieldArray.value = field.value;
                            fieldArray.name = field.getAttribute('name');
                            field.classList.remove('error');
                            if( fieldGroup ) fieldGroup.classList.remove('error');
                            fieldsArray[e] = fieldArray;
                        }
                    });

                    fieldsArray = JSON.stringify(fieldsArray);
                    
                    formData.append( 'action', 'form_validation' );
                    formData.append( 'security', securityField.value );
                    formData.append( 'fields', fieldsArray );

                    xhr.open('POST', ajaxurl, true);
                    xhr.onload = function () {
                        let responseArray = JSON.parse(this.response),
                            message       = responseArray.data.text,
                            url           = decodeURIComponent(responseArray.data.url),
                            formSubmit    = form.querySelector('.steps__nav');

                        if (this.status >= 200 && this.status < 400) {

                            form.reset();

                            formSubmit.insertAdjacentHTML('afterend', '<div class="form__response"><div class="form__success">' + message + '</div></div>' );
                            submitField.classList.add('disabled');

                            step1.classList.add('active');
                            step2.classList.remove('active');
                            prevStep.classList.add('disabled');
                            nextStep.classList.add('disabled');
                            stepsNav.classList.remove('step__2');
                            stepsNav.classList.add('step__1');

                            if( url ) window.open(url, '_self');

                        } else if( this.status >= 400 && this.status < 500 ){
                            let responseArray = JSON.parse(this.response),
                                fields        = responseArray.data,
                                field;

                            fields.forEach(function(resField){
                                if(resField.error){
                                    field = form.querySelector('[name="' + resField.name + '"]');
                                    field.classList.add('error');
                                    field.closest('.field__group').classList.add('error');
                                    field.insertAdjacentHTML('afterend', '<span class="field__error">' + resField.error + '</span>' );
                                }
                            });
                        } else {
                            formSubmit.insertAdjacentHTML('afterend', '<div class="form__response"><div class="form__error">' + message + '</div></div>' );
                        }
                    }
                    xhr.onerror = function() {
                        console.log('connection error');
                    };
                    xhr.send(formData);

                    submitField.classList.remove('load');
            });

            nextStep.addEventListener('click', function(){
                step1.classList.remove('active');
                step2.classList.add('active');
                stepsNav.classList.remove('step__1');
                stepsNav.classList.add('step__2');
                prevStep.classList.remove('disabled');
            });

            prevStep.addEventListener('click', function(){
                step1.classList.add('active');
                step2.classList.remove('active');
                stepsNav.classList.remove('step__2');
                stepsNav.classList.add('step__1');
                prevStep.classList.add('disabled');
            });
        }
    }

    contactForm(){
        let form = document.querySelector('.popup form');
        if( form ) {
            let formFields = form.querySelectorAll('input.form__field'),
                submitField = form.querySelector('button[type="submit"]'),
                value, type, fieldName, name, phone, email, label;

            formFields.forEach(function(field){
                field.addEventListener('keyup', function(formField){
                    value = formField.target.value;
                    fieldName = formField.target.getAttribute('name');
                    
                    switch(fieldName){
                        case 'name':
                            name = value;
                            break;
                        case 'phone':
                            phone = value;
                            break;
                        case 'email':
                            email = value;
                            break;
                    }

                    if(name && phone && email){
                        submitField.classList.remove('disabled');
                    } else {
                        submitField.classList.add('disabled');
                    }
                });
            })

            form.addEventListener('submit', function(){
                let formData           = new FormData(),
                    fields             = form.querySelectorAll('.form__field'),
                    fieldsError        = form.querySelectorAll('.field__error'),
                    formResponse       = form.querySelector('.form__response'),
                    securityField      = form.querySelector('input[name*="form-security"]'),
                    xhr                = new XMLHttpRequest(),
                    filesParam         = '',
                    fieldsArray        = {},
                    fieldArray, fieldGroup;

                    submitField.classList.add('load');

                    if(fieldsError.length){
                        fieldsError.forEach(function(field){
                            field.remove();
                        });
                    }
                    if(formResponse) formResponse.remove();

                    fields.forEach(function(field, e){
                        if( field.getAttribute('type') == 'radio' ){
                            if( field.checked ) {
                                fieldArray = {
                                    name: '',
                                    value: '',
                                    required: false
                                };
                                fieldGroup = field.closest('label');
                                fieldArray.required = (field.required) ? true : false;
                                fieldArray.value = field.value;
                                fieldArray.name = field.getAttribute('name');
                                field.classList.remove('error');
                                if( fieldGroup ) fieldGroup.classList.remove('error');
                                fieldsArray[e] = fieldArray;
                            }
                        } else {
                            fieldArray = {
                                name: '',
                                value: '',
                                required: false
                            };
                            fieldGroup = field.closest('label');
                            fieldArray.required = (field.required) ? true : false;
                            fieldArray.value = field.value;
                            fieldArray.name = field.getAttribute('name');
                            field.classList.remove('error');
                            if( fieldGroup ) fieldGroup.classList.remove('error');
                            fieldsArray[e] = fieldArray;
                        }
                    });

                    fieldsArray = JSON.stringify(fieldsArray);
                    
                    formData.append( 'action', 'form_validation' );
                    formData.append( 'security', securityField.value );
                    formData.append( 'fields', fieldsArray );

                    xhr.open('POST', ajaxurl, true);
                    xhr.onload = function () {
                        let responseArray = JSON.parse(this.response),
                            message       = responseArray.data.text,
                            url           = decodeURIComponent(responseArray.data.url),
                            formSubmit    = form.querySelector('.submit__block');

                        if (this.status >= 200 && this.status < 400) {

                            form.reset();

                            formSubmit.insertAdjacentHTML('afterend', '<div class="form__response"><div class="form__success">' + message + '</div></div>' );
                            submitField.classList.add('disabled');

                            if( url ) window.open(url, '_self');

                        } else if( this.status >= 400 && this.status < 500 ){
                            let responseArray = JSON.parse(this.response),
                                fields        = responseArray.data,
                                field;

                            fields.forEach(function(resField){
                                if(resField.error){
                                    field = form.querySelector('[name="' + resField.name + '"]');
                                    field.classList.add('error');
                                    field.closest('.field__group').classList.add('error');
                                    field.insertAdjacentHTML('afterend', '<span class="field__error">' + resField.error + '</span>' );
                                }
                            });
                        } else {
                            formSubmit.insertAdjacentHTML('afterend', '<div class="form__response"><div class="form__error">' + message + '</div></div>' );
                        }
                    }
                    xhr.onerror = function() {
                        console.log('connection error');
                    };
                    xhr.send(formData);

                    submitField.classList.remove('load');
            });
        }
    }

    partnersSection(){
        let section = document.querySelector('.partners__section');
        if( section ) {
            let partners = section.querySelector('.partners__block'),
                titleBlock = section.querySelector('.title__block'),
                windowHeight, topHeight, partnersHeight, partnersTopHeight, scrollHeight = 0;
            window.addEventListener('scroll', function() {
                windowHeight = window.innerHeight;
                partnersHeight = partners.getBoundingClientRect().height;
                partnersTopHeight = (-partners.getBoundingClientRect().top) + 450;
                topHeight = section.getBoundingClientRect().top;
                if( topHeight <= 0 ){
                    if( partnersTopHeight < partnersHeight ) titleBlock.style.top = (-topHeight) * 0.9 + 'px';
                } else {
                    titleBlock.style.top = '0px';
                }
            });
        }
    }

    rooomSection(){
        let section = document.querySelector('.rooom__section');
        if( section ) {
            let titleSection = section.querySelector('.title'),
                title = section.querySelector('.title h2'),
                contact = section.querySelector('.contact'),
                windowHeight, topHeight, sectionHeight, sectionTop, scale, scaleContact, opacity, marginLeft, background;
            window.addEventListener('scroll', function() {
                topHeight = section.getBoundingClientRect().top;
                sectionTop = - section.getBoundingClientRect().top;
                windowHeight = window.innerHeight;
                sectionHeight = section.getBoundingClientRect().height - windowHeight;
                if( topHeight <= 0 ){
                    scale = 1 + ( 7 * sectionTop ) / sectionHeight;
                    marginLeft = ( 300 * sectionTop ) / sectionHeight;
                    opacity = scaleContact = background = sectionTop / sectionHeight;

                    section.style.backgroundColor = `rgba(255,97,80,${background})`;
                    title.style['transform'] = `scale(${scale})`;
                    contact.style['transform'] = `scale(${scaleContact})`;
                    contact.style.opacity = opacity;
                    titleSection.style.marginLeft = marginLeft + 'px';

                    section.classList.add('fixed');
                } else {
                    title.style['transform'] = `scale(${1})`;
                    contact.style['transform'] = `scale(${0.6})`;
                    contact.style.opacity = 0;
                    section.style.backgroundColor = `rgba(255,97,80,0)`;
                    titleSection.style.marginLeft = 0 + 'px';

                    section.classList.remove('fixed');
                }
            })
        }
    }
}

let rooom = new GeneralClass();